package com.techx.pax;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.techx.pax")
public class Application extends SpringBootServletInitializer {

    protected Application() {
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}